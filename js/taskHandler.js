const TASKS = 'Tasks'

const addTaskToLocal = (taskInfo, isDone) => {
    const tasks = getLocalTasks()
    tasks.push({taskInfo, isDone})
    setLocalTasks(tasks)
}

const getLocalTasks = () => {
    const localTasks = localStorage.getItem(TASKS)
    return localTasks ? JSON.parse(localTasks) : []
}

const setLocalTasks = (tasks) => {
    localStorage.setItem(TASKS, JSON.stringify(tasks))
}

const initLocalTasks = () => {
    if(!localStorage.hasOwnProperty(TASKS)) {
        setLocalTasks([])
    }
}

const clearLocalTasks = () => {
    localStorage.removeItem(TASKS)
}

const deleteLocalTask = (taskInfo, isDone) => {
    const tasks = getLocalTasks().filter(
        (o) => o.taskInfo !== taskInfo || o.isDone !== isDone)
    setLocalTasks(tasks)
}

export {
    initLocalTasks,
    addTaskToLocal,
    getLocalTasks,
    clearLocalTasks,
    deleteLocalTask,
    setLocalTasks
}