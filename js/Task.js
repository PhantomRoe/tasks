import {initLocalTasks, getLocalTasks, addTaskToLocal, clearLocalTasks, deleteLocalTask, setLocalTasks} from './taskHandler.js'

window.onload = () => {
    init()

    getListElem().addEventListener('change',updateTasks)
    movingFunctionality()
    
    window.addTask = addTask
    window.clearTasks = clearTasks
    window.deleteTask = deleteTask
    window.hideDoneTasks = hideDoneTasks
}

const addTask = () => {
    const newTaskElem = document.getElementById('add')
    const taskInfo = newTaskElem.value
    newTaskElem.value = ""

    addTaskToLocal(taskInfo, false)
    getListElem().insertAdjacentHTML('beforeend', getTaskElement(taskInfo, false))
}

const clearTasks = () => {
    clearLocalTasks()
    getListElem().innerHTML = ""
}

const deleteTask = (task) => {
    const parent = task.parentNode
    parent.removeChild(task)
    deleteLocalTask(getTaskInfo(task),
    getIsDone(task))
}

const hideDoneTasks = () => {
    const tasks = getTasks()
    const checkedTasks = tasks.filter(task => getIsDone(task))

    if(checkedTasks.length > 0) {
        checkedTasks.forEach((task) => task.classList.toggle('hidden'))

        const hideDone = 'Hide Done Tasks'
        const showAll = 'Show all'
        
        const doneButton = document.getElementById('doneButton')
        if(doneButton.innerHTML != hideDone) {
            doneButton.innerHTML = hideDone
        } else {
            doneButton.innerHTML = showAll
        }
    }
}

const updateTasks = () => {
    console.log('updated!!')
    const tasks = getTasks()
    
    setLocalTasks(tasks.map(task => (
        {taskInfo: getTaskInfo(task), isDone: getIsDone(task)}
    )))
}

const init = () => {
    initLocalTasks()
    for(let task of getLocalTasks()) {  
        getListElem().innerHTML += getTaskElement(task.taskInfo, task.isDone)
    }
}

const getTasks = () => {
    return [...getListElem().getElementsByClassName('task')];
}

const getListElem = () => {
    return document.getElementById('tasks')
}

const getTaskElement = (taskInfo, isDone) => {
    return `<div class="task">
                <div class="imgContainer edgeContainer leftContainer move">
                    <img src="images/Dots.png">
                </div>
                <div class="taskBody">
                    <input class="isDone" type="checkbox" ${isDone ? 'checked' : ''}>      
                    <input class="taskInfo" type="text" readonly value="${taskInfo}">
                </div>
                <div class="imgContainer edgeContainer rightContainer" onclick="deleteTask(this.parentNode)">
                    <img src="images/delete.png">
                </div>
            </div>`
}



const movingFunctionality = () => {
    getTasks().forEach(
        (task) => {
            const moveElem = task.getElementsByClassName('move')[0]
                moveElem.addEventListener('mousedown', (event) => onTaskMouseDown(task, event))
        }
    )
}
let currentDroppable = null;

const onTaskMouseDown = (task, event) => {
    let shiftX = event.clientX - task.getBoundingClientRect().left + 10;
    let shiftY = event.clientY - task.getBoundingClientRect().top + 10;

    task.classList.add('popOutTask')

    moveAt(event.pageX, event.pageY);

    function moveAt(pageX, pageY) {
        task.style.left = pageX - shiftX + 'px';
        task.style.top = pageY - shiftY + 'px';
    }

    function getBellow(task, event) {
        task.classList.add('visible')
        let elemBelow = document.elementFromPoint(event.clientX, event.clientY);
        task.classList.remove('visible')
       
        if (!elemBelow) return;

        return elemBelow.closest('.task') || elemBelow.closest('.placeHolder')
    }

    currentDroppable = getBellow(task, event)
    currentDroppable.before(getPlaceHolder())
    getPlaceHolder().classList.add('placeHolder')
    
    function onMouseMove(event) {
        moveAt(event.pageX, event.pageY);

        let belowElement = getBellow(task, event)
        if (belowElement && currentDroppable != belowElement) {
            currentDroppable = belowElement
            const belowElementBound = belowElement.getBoundingClientRect()
            const placeHolderBound = getPlaceHolder().getBoundingClientRect()
        
            if(belowElementBound.bottom > placeHolderBound.bottom
                || belowElementBound.left > placeHolderBound.left) {
                belowElement.after(getPlaceHolder())
        
            } else {
                belowElement.before(getPlaceHolder())
            }  
        }
      }
    document.addEventListener('mousemove', onMouseMove);

    task.onmouseup = function() {
        document.removeEventListener('mousemove', onMouseMove);

        task.classList.remove('popOutTask')
        getPlaceHolder().classList.remove('placeHolder')

        getPlaceHolder().before(task)
        
        updateTasks()
        task.onmouseup = null;
    };
}

const getPlaceHolder = () => {
    return document.getElementById('placeHolder')
}

const getTaskInfo = (task) => {
    return task.getElementsByClassName('taskInfo')[0].value
}

const getIsDone = (task) => {
    return task.getElementsByClassName('isDone')[0].checked
}